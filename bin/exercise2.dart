import 'dart:io';

void main() {
  int count = 1;
  String? message =
      'The final keyword in Dart is used to create constants or objects that are immutable in nature.';
  var chagetoLow = message.toLowerCase();
  var words = chagetoLow.split(" ");
  List<String> wordsList = [];
  List<int> count_word = [];
  for (int i = 0; i < words.length; i++) {
    var duplicate = false;
    for (int j = 0; j < wordsList.length; j++) {
      if (words[i] == wordsList[j]) {
        duplicate = true;
        count_word[j] = count_word[j] + 1;
        break;
      }
    }
    if (duplicate == false) {
      wordsList.add(words[i]);
      count_word.add(1);
    }
  }
  for (var i = 0; i < wordsList.length; i++) {
    print("${wordsList[i]} ${count_word[i]}");
  }
}
